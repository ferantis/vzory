package main.simpleFactoryMethod;

abstract public class Human {

    public static void main(String[] args) {
        for( int i=1; i <= 3; i++ ) {
            Human human = getHuman();
            System.out.println( "\n=== new Human: " + human.getClass().getSimpleName());
            human.wakeUp();
            human.work();
            human.freeTime();
            human.sleep();
        }
    }

    private static int index = 0;

    public static Human getHuman() {
        switch( index++ % 3 ) {
            case 0: return new LazyHuman();
            case 1: return new Student();
            case 2: return new Worker();
            default: throw new RuntimeException( "invalid index" );
        }
    }
    abstract public void wakeUp();
    abstract public void work();
    abstract public void freeTime();
    abstract public void sleep();


    private static class LazyHuman extends Human {
        public void wakeUp() { System.out.println("Slowly wake up"); }
        public void work()
        { System.out.println("working slowly"
        ); }
        public void freeTime()
        { System.out.println("long free time" ); }
        public void sleep() { System.out.println("long sleep"
        ); }
    }
    private static class Student extends Human {
        public void wakeUp() { System.out.println("wake up"
        ); }
        public void work()
        { System.out.println("working"
        ); }
        public void freeTime()
        { System.out.println("free time" ); }
        public void sleep() { System.out.println("sllep"
        ); }
    }
    private static class Worker extends Human {
        public void wakeUp() { System.out.println("early vake up"
        ); }
        public void work()
        { System.out.println("working hard" ); }
        public void freeTime()
        { System.out.println("no free time"
        ); }
        public void sleep() { System.out.println("short sleep" ); }
    }

}
