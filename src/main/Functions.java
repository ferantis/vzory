package main;

public class Functions {

    private Functions() {}

    public static int greatestCommonDivider(int i1, int i2)
    {
        i1 = Math.abs( i1 );
        i2 = Math.abs( i2 );
        while( i2 > 0 )
        {
            int pom = i1 % i2;
            i1 = i2;
            i2 = pom;
        }
        return i1;
    }

    public static int lowestCommonTimer(int i1, int i2)
    {
        if( (i1 == 0) || (i2 == 0) )
            return 0;
        return i2 * Math.abs(i1) / greatestCommonDivider(i1,i2);
    }
}
