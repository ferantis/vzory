package main.singleton;

public abstract class Maze {

    public static Maze maze;

    public static Maze getInstance() {
        if( maze == null ) {
            synchronized( Maze.class ) {
                if( maze == null ) {
                    String name = System.getenv( "Maze" );
                    if( name == null ) name = "";
                    maze = chooseMaze( name );
                }
            }
        }
        return maze;
    }

    private Maze() {}

    private static Maze chooseMaze( String name ) {
        if( name.equalsIgnoreCase( "BOMB" ) )
            return new BombMaze();
        else if( name.equalsIgnoreCase( "MAGIC" ) )
            return new MagicMaze();
        else if( name.equalsIgnoreCase( "FUNNY" ) )
            return new FunnyMaze();
        else
            return new ImplicitMaze();
    }

    private static class BombMaze
            extends Maze {}
    private static class MagicMaze
            extends Maze {}
    private static class FunnyMaze
            extends Maze {}
    private static class ImplicitMaze extends Maze {}
}
