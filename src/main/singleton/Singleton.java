package main.singleton;

public class Singleton {
    private static final Singleton instance = new Singleton();
    private String name;

    private Singleton() {
        this.name = "My awesome name";
    }
    
    public static Singleton getInstance() {
        return instance;
    }
}
