package main.enumType;

/*******************************************************************************
 * Enum type for 1-4 java version
 */
public class DirectionOld {

    private static final DirectionOld[] DIRECTION = new DirectionOld[4];
    private static int DIRECTIONS = 0;

    public static final DirectionOld
            EAST = new DirectionOld( 1, 0, "EAST"),
            NORTH = new DirectionOld( 0,-1, "NORTH" ),
            WEST = new DirectionOld(-1, 0, "WEST" ),
            SOUTH = new DirectionOld( 0, 1, "SOUTH");

    private final int dx, dy;
    private final int position;
    private final String name;

    private DirectionOld( int dx, int dy, String name ){
        this.dx
                = dx;
        this.dy
                = dy;
        this.name = name;
        position = DIRECTIONS++;
        DIRECTION[ position ] = this;
    }

    public String toString() {
        return name;
    }

    public int dx() {
        return dx;
    }

    public int dy() {
        return dy;
    }

    public DirectionOld turnLeft() {
        return DIRECTION[ (position+1) % DIRECTIONS ];
    }

    public DirectionOld turnAround() {
        return DIRECTION[ (position+2) % DIRECTIONS ];
    }
}
