package main.enumType;

public enum Direction {
    EAST(1,0), NORTH(0,-1), WEST(-1,0), SOUTH(0,1);

    private final int dx, dy;

    private Direction( int dx, int dy ) {
        this.dx = dx;
        this.dy = dy;
    }
}
