package main.enumType;

public enum FunctionalEnum {
    LAZY {
        public void wakeUp() { System.out.println("wake up slowly" ); }
        public void work() { System.out.println("work slowly"); }
        public void freeTime() { System.out.println("slowly free time" ); }
        public void sleep() { System.out.println("always sleep"); }
    },
    NORMAL {
        public void wakeUp() { System.out.println("wake up normal"); }
        public void work() { System.out.println("work normal"); }
        public void freeTime() { System.out.println("normal rest" ); }
        public void sleep() { System.out.println("normal sleep"); }
    },
    WORKER {
        public void wakeUp() { System.out.println("wake up early"); }
        public void work() { System.out.println("work hard" ); }
        public void freeTime() { System.out.println("always work"); }
        public void sleep() { System.out.println("don`t sleep" ); }
    };

    abstract public void wakeUp();
    abstract public void work();
    abstract public void freeTime();
    abstract public void sleep();
}
