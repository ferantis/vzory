package main.immutableObjects;

import java.util.HashSet;
import java.util.Set;

public class BadDivide2 extends BadDivide{

    public static void main(String[] args) {
        BadDivide2 bda = new BadDivide2( 2, 1 );
        BadDivide2 bdb = bda;
        BadDivide2 other = new BadDivide2( 2, 1 );
        Set<BadDivide2> set = new HashSet<BadDivide2>();
        set.add(bda);
        System.out.println("Presence in set: bda-" + set.contains(bda) + ", other-" + set.contains(other) );
        System.out.println("Before operation: bda=" + bda + ", bdb=" + bdb);
        bdb = (BadDivide2)bdb.times( bdb );
        System.out.println("After operation:bda=" + bda + ", bdb=" + bdb);
        System.out.println("Presence in set: bda-" + set.contains(bda) +", other-" + set.contains(other) );
    }
    
    public BadDivide2(int base, int divider) {
        super(base, divider);
    }

    public boolean equals( Object o ) {
        if( o instanceof BadDivide ) {
            BadDivide bd = (BadDivide)o;
            return (base == bd.base) && (divider == bd.divider);
        }
        return false;
    }

    public int hashCode()
    {
        return (717 + base) *37 + divider;
    }
}
