package main.immutableObjects;

import java.awt.event.AdjustmentEvent;

public class BadDivide {

    public static void main(String[] args) {
        BadDivide bda = new BadDivide( 2, 1 );
        BadDivide bdb = bda;
        System.out.println("Before: bda=" + bda + ", bdb=" + bdb );
        bdb = bdb.times( bdb );
        System.out.println("After: bda=" + bda + ", bdb=" + bdb );
    }
    protected int base, divider;

    public BadDivide(int base, int divider) {
        this.base = base;
        this.divider = divider;
    }

    public BadDivide times( BadDivide badDivide ) {
        base *= badDivide.base;
        divider *= badDivide.divider;
        return this;
    }

    public String toString() {
        return "[" + base + "/" + divider + "]";
    }
}
