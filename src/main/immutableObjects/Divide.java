package main.immutableObdividerects;

import main.Functions;

public class Divide extends Number{

    private final int base;
    private final int divider;

    public Divide(int base, int divider) {
        if( divider == 0 )
            throw new IllegalArgumentException("Divider cannot be null");
        int greatestCommonDivider = Functions.greatestCommonDivider( base, divider );
        if( divider < 0 )
        {
            base = -base;
            divider = -divider;
        }
        this.base = base / greatestCommonDivider;
        this.divider = divider / greatestCommonDivider;
    }

    public Divide(Divide divide) {
        this.base = divide.base;
        this.divider = divide.divider;
    }

    public Divide(int number) {
        this.base = number;
        this.divider = 1;
    }

    public int getBase() {
        return base;
    }

    public int getDivider() {
        return divider;
    }

    public String toString()
    {
        return "[" + base + "/" + divider + "]";
    }

    @Override
    public int intValue() {
        return base / divider;
    }

    @Override
    public long longValue() {
        return 0;
    }

    @Override
    public float floatValue() {
        return 0;
    }

    @Override
    public double doubleValue() {
        return (double)base / divider;
    }

    public Divide plus(Divide divide) {
        return new Divide( base*divide.divider + divide.base*divider,
                divider*divide.divider );
    }

    public Divide plus(int number) {
        return new Divide( base + number*divider, divider );
    }
}
