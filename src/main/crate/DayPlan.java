package main.crate;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class DayPlan {
    private final List<Item> actions = new ArrayList<>();

    public boolean add( int hour, int minute, int duration, String subject ) {
        int start = hour * 60 + minute;
        int end = start + duration;
        ListIterator<Item> lip = actions.listIterator();
        while( lip.hasNext() ) {
            Item item = lip.next();
            if( item.end <= start ) {
                continue;
            }
            if( item.start <= end ) {
                return false;
            }
        }
        lip.add( new Item( start, end, subject ) );
        return true;
    }

    public void print() {
        System.out.println("\nSet of actions:");
        for( Item p : actions) {
            System.out.printf("%02d:%02d - %02d:%02d %s\n",
            p.start /60, p.start %60, p.end /60, p.end %60, p.subject );
        }
        System.out.println("——————————————————-");
    }

    private static class Item {
        int start;
        int end;
        String subject;
        Item(int start, int end, String subject) {
            this.start = start;
            this.end = end;
            this.subject = subject;
        }
    }

    public void testAdd( int h, int m, int t, String txt ) {
        boolean success;
        System.out.printf("Adding %d minutes from %02d:%02d for %s\n", t, h, m, txt);
        success = add( h, m, t, txt);
        System.out.println(" " + (success? "YES" : "NO") );
        print();
    }

    public static void main(String[] args) {
        DayPlan dp = new DayPlan();
        dp.testAdd( 8, 0, 30, "Wake up");
        dp.testAdd(10, 30, 90, "Relax" );
        dp.testAdd( 8, 30, 30, "Breakfast" );
        dp.testAdd( 9, 30, 90, "Work"
        );
    }
}
